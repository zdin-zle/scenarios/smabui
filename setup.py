from setuptools import setup

setup(
    name='ZDIN-ZLE-SmaBUI',
    version='0.1',
    author='Henrik Wagner and Christian Reinhold',
    author_email='henrik.wagner/c.reinhold at tu-braunschweig.de',
    description=('SmaBUI is an easy to use co-simulation of a building energy system using mosaik.'),
    long_description=(open('README.rst').read() + '\n\n' +
                      open('CHANGES.txt').read() + '\n\n' +
                      open('AUTHORS.txt').read()),
    url='https://gitlab.com/zdin-zle/scenarios/smabui',
    install_requires=[
        'arrow',
        'numpy',
        'pandas',
        'mosaik>=3.0'
    ],
    include_package_data=True,
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Console',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: GNU Lesser General Public License v2 (LGPLv2)',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Topic :: Scientific/Engineering',
    ],
    )
