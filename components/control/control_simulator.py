"""
Mosaik interface for the control simulator.

@author: Christian Reinhold and Henrik Wagner elenia@TUBS
"""


import mosaik_api
from components.control import control

# SIMULATION META DATA
META = {
    'type': 'time-based',
    'models': {
        'BEM': {
            'public': True,
            'params': ['init_vals'],
            'attrs': ['P_SET','P_LOAD','Q_LOAD', 'P_PV', 'P_MAX','P_MIN', 'P_SALDO', 'P_SALDO_RES', 'RES_SIGNAL',
            'PROGNOSIS_START', 'PROGNOSIS_END',
            'SIGNAL_PROGNOSIS_CS_control','SIGNAL_PROGNOSIS','PROGNOSIS_LOAD','PROGNOSIS_PV','PROGNOSIS_RESIDUAL'
            ]
        },
        'AEM': {
            'public': True,
            'params': [],
            'attrs': []
        }
    },
    "extra_methods": [
        "add_controlled_entity","get_entities"
    ]
}

# ------------INPUT-SIGNALS--------------------
# P_PV                  Actual Active Power PV [W]
# P_LOAD                Active Power Load [W]
# Q_LOAD                Reactive Power Load [var]
# P_MAX                 Maximal active power [W]
# P_MIN                 Minimal active power  [W]

# ------------OUTPUT-SIGNALS--------------------
# P_SET                 Set-Point for components [W]

class Sim(mosaik_api.Simulator):
    """Simulator class for mosaik-control model

    Args:
        mosaik_api (module): defines communication between mosaik and simulator

    Raises:
        ValueError: Unknown output attribute, when not described in META of simulator
    """

   # contructor
    def __init__(self):
        """Constructor
        """

        super(Sim, self).__init__(META)
        self.step_size = None
        self.entities = {}

        # assign simulator
        self.simulator = control.Simulator()        

    # init API
    def init(self, sid, time_resolution, step_size = 1):
        """Initializer

        Args:
            sid (str): Id of the created instance of the load simulator (e.g. ControlSim-0)
            time_resolution (float): Time resolution of current mosaik scenario.
            step_size (int, optional): Simulation step size. Defaults to 1.

         Returns:
             meta: meta describing the simulator 
        """

        # assign properties
        self.sid = sid
        self.step_size = step_size

        # return meta data
        return self.meta

    # create model
    def create(self, num, model_type, init_vals = None):
        """Creates instances of the mosaik-control model

        Args:
            num (int): Number of control models to be created
            model_type (str): Description of the created mosaik-control instance (here "Control")
            init_vals (list): List (length=num) with initial values for each control model.
            Defaults to None

        Returns:
            dict: return created entities
        """

        # get next id
        next_eid = len(self.entities)

        # create list for entities
        entities = []

        # loop over all entities
        for i in range(next_eid, next_eid + num):

            # create name of model
            ename = '%s%s%d' % (model_type, '_', i)

            # create new model with init values
            model = self.simulator.add_model(model_type, ename, init_vals[i] if init_vals is not None else None)

            # add model information
            self.entities[ename] = {'ename': ename, 'etype': model_type, 'model': model}

            # append entity to list
            entities.append({'eid': ename, 'type': model_type})

        # return created entities
        return entities

    # perfom calculation step
    def step(self, time, inputs, max_advance):
        """Performs simulation step calling the mosaik-control model

        Args:
            time (int): Current simulation time according to step size. Given by MOSAIK.
            inputs (dict): Is a dictionary, that maps entity IDs to 
            data dictionaries which map attribute names to lists of values. Given by MOSAIK.
            max_advance (int, optional): Is the simulation time until the simulator can safely advance it's internal time without causing any causality errors.

        Returns:
            int: New time stamp (time increased by step size)
        """

        # loop over all models
        for eid, attrs in inputs.items():
            # loop over all attributes
            for attr, values in attrs.items():                
                # assign properties with sid
                setattr(self.entities[eid]["model"],attr,attrs.get(attr, {}))

        # perform simulation step
        self.simulator.step(time)

        # set data element
        data = self.simulator.get_set_data(inputs)        

        # send data to other simulators/models
        yield self.mosaik.set_data(data)

        # return actual time for mosaik
        return time + self.step_size

    # get data
    def get_data(self, outputs):
        """Gets the data for the next concatenated model

        Args:
            outputs (dict): Dictionary with data outputs from each control model

        Raises:
            ValueError: Error if attribute not in model metadata

        Returns:
            dict: Dictionary with simulation outputs
        """

        # create output data
        data = {}

        # loop over all output values
        for ename, attrs in outputs.items():
            # get model entry
            entry = self.entities[ename]

            # set data entry for model
            data[ename] = {}

            # loop over all attributes
            for attr in attrs:
                if attr not in self.meta['models'][entry["model"].type]['attrs']:
                    raise ValueError('Unknown output attribute: %s' % attr)

                # Get model data
                data[ename][attr] = getattr(entry["model"], attr)
        
        # return data to mosaik
        return data

    # add component to control unit
    def add_controlled_entity(self, control_unit, model):
        """ Adds entities (e.g. pvs entities) to the control unit   

        Args:
            control_unit (list): List with control unit entities
            model (dict): Dictionary of created models to be added to the control unit entities
        """
        # add controlled component to BEM
        self.simulator.add_controlled_entity(control_unit, model)        

    # get entities
    def get_entities(self):
        """Provides a list with description of the entities

        Returns:
            list: List with description of the entities
        """

        # return entities of API
        return self.entities    


def main():
    """Test function for connectivity

    Returns:
        sim: A MOSAIK simulation 
    """

    return mosaik_api.start_simulation(Sim())
