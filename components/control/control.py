"""
@author: Christian Reinhold and Henrik Wagner elenia@TUBS

The mosaik-control was developed as
an energy management system on the building level and is
therefore connected to all of the building's components. The
control gathers all of the component's power flows, calculates
the energy balance at the building node and the component's
flexibility, and then sends out power set values in each step.
Hereby, available solar power is favored to grid consumption,
as well as charging/discharging limits of the charging stations,
and batteries are considered.

"""

#default BEM:
class BEM:
    """Models an energy managament system on building level
    """
    # static properties
    type = 'BEM'                            # model type
    components = []                         # controlled components by control unit [-]

    # dynamic input properties
    P_LOAD = None                           # Active Power Load [W]
    Q_LOAD = None                           # Reactive Power Load [var]
    P_PV = None                             # Active Power PV [W]
    P_MIN = None                            # Maximum active power [W]
    P_MAX = None                            # Maximum reactive power [W]
    SIGNAL_PROGNOSIS_CS_control = None      # Signal to request new forecast period based on BEV_consumption_period [-]
    BEV_consumption_period = None           # Electricity consumption of EV in next period not being home, 
                                            # starting right after STANDING_TIME_END
    PROGNOSIS_START = None                  # Starting time of forecast horizon
    PROGNOSIS_END = None                    # End time of forecast horizon
    PROGNOSIS_LOAD = None                   # Load prognosis for forecast horizon
    PROGNOSIS_PV = None                     # PV prognosis for forecast horizon
    RES_SIGNAL = None                       # helper signal to check if surplus energy is available

    # dynamic output properties
    P_SET = {}                              # Set-Point for components [W]
    P_SALDO = None                          # Saldo active power [W]
    P_SALDO_RES = None                      # Residual renewable power [W]
    SIGNAL_PROGNOSIS = None                 # Output signal to request prognosis from load/pv
    PROGNOSIS_RESIDUAL = {}                 # Residual prognosis calculated by load & pv prognoosis [Wh]

    # result properties
    results = ['P_SET', 'P_SALDO']

    # set data identifier
    set_data = ['P_MIN', 'SIGNAL_PROGNOSIS','PROGNOSIS_START','PROGNOSIS_END','PROGNOSIS_RESIDUAL']

    # init of values
    def __init__(self, init_vals):
        """Initializes the mosaik-control model

        Args:
            init_vals (dict, optional): Initial values for creation of BEM model. Normally not used.
        """

        # assign init values
        if init_vals:
            for i, (key, value) in enumerate(init_vals.items()):
                setattr(self, key, value)

    # perform simulation step
    def step(self, time):
        """Gathers current energy flows, estimates energy balances and 
        then calculates power set values for each connected component.

        Args:
            time (int): Current simulation time
        """        

        # set active power saldo
        self.P_SALDO = sum(self.P_LOAD.values())

        # PROGNOSIS: manage signal exchange between household and charging stations
        for i, _ in self.eid["household"].items():

            # loop over connected CS 
            for j, _ in self.eid["charging_station"].items():
                # TODO: only works with one connected CS station
                # set signal for prognosis in connected loads
                self.SIGNAL_PROGNOSIS[i] = self.SIGNAL_PROGNOSIS_CS_control[j]
                self.PROGNOSIS_START[i] = self.PROGNOSIS_START[j]
                self.PROGNOSIS_END[i] = self.PROGNOSIS_END[j]
                

        # loop over all pv systems
        for i, attr in self.eid["PV"].items():

            # assign actual uncontrolled active power
            self.P_SALDO += self.P_PV[i]

            # set control value
            self.P_SET[i] = self.P_PV[i]

            # PROGNOSIS: set signal for prognosis (from CS) in connected PVs
            for j, _ in self.eid["charging_station"].items():
                self.SIGNAL_PROGNOSIS[i] = self.SIGNAL_PROGNOSIS_CS_control[j]
                self.PROGNOSIS_START[i] = self.PROGNOSIS_START[j]
                self.PROGNOSIS_END[i] = self.PROGNOSIS_END[j]   
        
        # loop over all charging stations
        for i, attr in self.eid["charging_station"].items():

            # set set point
            self.P_SET[i] = self.P_MAX[i]

            # assign actual uncontrolled active power
            self.P_SALDO += self.P_SET[i]
            
            # check for renewable power surplus
            if self.P_SALDO >= 0:
                self.RES_SIGNAL[i] = False
            elif self.P_SALDO < 0:
                self.RES_SIGNAL[i] = True
            # save renewable power surplus
            self.P_SALDO_RES[i] = self.P_SALDO

            # PROGNOSIS: calculating the prognosis for residual load without considering battery storage
            for household, load_prognosis in self.PROGNOSIS_LOAD.items():
                for pv, pv_prognosis in self.PROGNOSIS_PV.items():
                    if load_prognosis and pv_prognosis is not None:
                        self.PROGNOSIS_RESIDUAL[i] = float(load_prognosis - pv_prognosis)
                    else:
                        self.PROGNOSIS_RESIDUAL[i] = float(0)   

        # loop over all storage systems
        for i, attr in self.eid["eStorage"].items():

            # check if too much load
            if self.P_SALDO > 0:
                self.P_SET[i] = -min(self.P_SALDO, self.P_MIN[i])
            elif self.P_SALDO < 0:
                self.P_SET[i] = min(abs(self.P_SALDO), self.P_MAX[i])
            elif self.P_SALDO == 0:
                self.P_SET[i] = 0

            # saldo
            self.P_SALDO += self.P_SET[i]

    # update internal structure for components
    def update_controlled_entity(self):
        """Updates the connected/controlled components of the BEM
        """     
        # clear storage components
        self.eid = {'PV': {},'eStorage': {}, 'charging_station': {}, 'household': {}}

        # get all component types
        for component in self.components:
            for attr, values in component.items():
                if values["etype"] not in self.eid:
                    self.eid[values["etype"]] = {}
                self.eid[values["etype"]][values["full_id"]] = values

    # get component by type
    def get_component_type(self, type):
        """Retrieve all components

        Args:
            type (any): Input to determine all components of this type.

        Returns:
            dict: Returns all component of input "type"
        """

        # init data element
        data = {}

        # get all component types
        for component in self.components:
            for attr, values in component.items():
                if values["etype"] == type:
                    data[attr] = values

        return data
    
class AEM:
    """Template to create an apartment energy management system

    Returns:
        _type_: Name of energy management system (apartment energy management system)
    """
    
    # init of values
    def __init__(self, init_vals):
        """Initializes the mosaik-control model

        Args:
            init_vals (dict, optional): Initial values for creation of AEM model
        """
        # assign init values
        if init_vals:
            for i, (key, value) in enumerate(init_vals.items()):
                setattr(self, key, value)
                

    def step(self,time):
        """Sets the simulation time.

        Args:
            time (int): Current simulation time
        """

        self.time = time 

class Simulator(object):
    """Simulator class for mosaik-control model
    """

    def __init__(self):
        """Constructor
        """

        # init data elments
        self.models = {}

    # create model instance
    def add_model(self, model_type, model_name, init_vals):
        """Creates instance of control model, called from control_simulator

        Args:
            model_type (str): Describing the model type
            model_name (str): Name of model consists of type and number (e.g. BEM_0)
            init_vals (dict, optional): List (length=num) with initial values for each control model. 
            Optional for BEM model

        Returns:
            model: returns created control object
        """

        # create model with init values
        if model_type == 'BEM':
            model = BEM(init_vals)
        elif model_type == 'AEM':
            model = AEM(init_vals)
        elif model_type == 'BEM_APT':
            model = BEM_APT(init_vals)

        # add model to model storage
        self.models[model_name] = {'name': model_name, 'type': model_type,'model': model, 'results': []}

        # return model class
        return model

    # add controlled unit
    def add_controlled_entity(self, model, component):
        """Adds a controlled component to the respective controller

        Args:
            models (dict): Input list with all models (model_names) of type BEM/AEM/BEM-APT, which need to
            be filtered for correct controller.
            component (dict): Input list with all models of a type (e.g. charging_station) which
            will be connected to the controller.
        """
        
        #self.models[model.eid]["model"].components = []
        if isinstance(model,list):
            # append model to list
            for i in range(len(model)):
                self.models[model[i].eid]["model"].components.append(component)
            # update model type assignment
                self.models[model[i].eid]["model"].update_controlled_entity()
        else:
           # append model to list
            self.models[model.eid]["model"].components=[component]
            # update model type assignment
            self.models[model.eid]["model"].update_controlled_entity()


     # perform simulation step
    def step(self, time):
        """Performs simulation step of all to simulator connected models

        Args:
            time (int): Current simulation time.
        """
        
        # Enumaration over all models in simulator
        for i, model_entry in self.models.items():
            # perform simulation step
            model_entry["model"].step(time)

            # collect data of model and storage local
            for j, signal in enumerate(model_entry["model"].results):
                model_entry["results"].append(getattr(model_entry["model"],signal)) 

    # get data for set data
    def get_set_data(self, inputs):
        """Collects all processed data/values from control class and sents them to controlled entities.
        Most importantly these are power set values, e.g. P_SET. 

        Args:
            inputs (dict): All mosaik-control models and their exchanged values (according to META)

        Returns:
            dict: Returns dict (data) with all set values for all BEM, 
            e.g. {BEM_0:{CSSim-0.charging_station_0: {P_SET : 0, RES_Signal: False}}} 
        """
        
        # create data element
        data = {}

        # loop over all models
        for eid, attrs in inputs.items():

            # get model entry
            model_entry = self.models[eid]

            # # get signal partner
            # signal_partners = attrs.get(model_entry["model"].set_data, {})

            # collect signal partners
            signal_partners = {}
            for i in range(len(model_entry["model"].set_data)):
                signal_partners.update(attrs.get(model_entry["model"].set_data[i], {}))

            # loop over all signal partners
            for model_eid, value in signal_partners.items():

                # create entry
                if eid not in data:
                    data[eid] = {}
                if eid not in data[eid]:
                    data[eid][model_eid] = {}
                
                # get signals for household prognosis
                if model_eid[0:4] == 'Load' or model_eid[0:2] == 'PV':
                    data[eid][model_eid]['SIGNAL_PROGNOSIS'] = model_entry["model"].SIGNAL_PROGNOSIS[model_eid]
                    data[eid][model_eid]['PROGNOSIS_START'] = model_entry["model"].PROGNOSIS_START[model_eid]
                    data[eid][model_eid]['PROGNOSIS_END'] = model_entry["model"].PROGNOSIS_END[model_eid]
                else:
                    # get values
                    data[eid][model_eid]['P_SET'] = model_entry["model"].P_SET[model_eid]
                
                # prepare signal exchange from/to charging station
                if model_eid[0:5] == 'CSim':
                    data[eid][model_eid]['RES_SIGNAL'] = model_entry["model"].RES_SIGNAL[model_eid]
                    data[eid][model_eid]['P_SALDO_RES'] = model_entry["model"].P_SALDO_RES[model_eid] #have to change for the more smarter solar charger
                    data[eid][model_eid]['SIGNAL_PROGNOSIS_CS_control'] = model_entry["model"].SIGNAL_PROGNOSIS_CS_control[model_eid]
                    data[eid][model_eid]['PROGNOSIS_RESIDUAL'] = model_entry["model"].PROGNOSIS_RESIDUAL[model_eid]
                    
        return data