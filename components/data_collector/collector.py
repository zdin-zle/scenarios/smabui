"""
A simple data collector that prints all data when the simulation finishes.

@author: Christian Reinhold and Henrik Wagner elenia@TUBS

"""


import collections
import mosaik_api

META = {
    'type': 'event-based',
    'models': {
        'Monitor': {
            'public': True,
            'any_inputs': True,
            'params': [],
            'attrs': [],
        },
    },
}

class Collector(mosaik_api.Simulator):
    """Simulator class for mosaik-collector

    Args:
        mosaik_api (module): defines communication between mosaik and simulator
    """

    def __init__(self):
        """Constructor
        """

        super().__init__(META)
        self.eid = None
        self.data = collections.defaultdict(lambda:
                                            collections.defaultdict(dict))

    def init(self, sid, time_resolution):
        """Initializer 

        Args:
            sid (str): Id of the created instance of the collector
            time_resolution (float): Time resolution of current mosaik scenario.

        Returns:
            meta: meta describing the simulator
        """

        return self.meta

    def create(self, num, model):
        """Creates instances of the mosaik-collector model

        Args:
            num (int): Number of collector models to be created (cannot be more than 1)
            model (str): Description of the created mosaik-collector instance

        Raises:
            RuntimeError: Can only create one instance of Monitor.

        Returns:
            List: return created collector
        """

        if num > 1 or self.eid is not None:
            raise RuntimeError('Can only create one instance of Monitor.')

        self.eid = 'Monitor'
        return [{'eid': self.eid, 'type': model}]

    def step(self, time, inputs, max_advance):
        """Performs simulation step calling the mosaik-pv model

        Args:
            time (int): Current simulation time according to step size. Given by MOSAIK.
            inputs (dict): Is a dictionary, that maps entity IDs to 
            data dictionaries which map attribute names to lists of values. Given by MOSAIK.
            max_advance (int, optional): Is the simulation time until the simulator can safely advance it's internal time without causing any causality errors.

        Returns:
            None: None object
        """

        data = inputs.get(self.eid, {})
        for attr, values in data.items():
            for src, value in values.items():
                self.data[src][attr][time] = value

        return None

    def finalize(self):
        """Prints all collected data to the console
        """

        print('Collected data:')
        for sim, sim_data in sorted(self.data.items()):
            print('- %s:' % sim)
            for attr, values in sorted(sim_data.items()):
                print('  - %s: %s' % (attr, values))


if __name__ == '__main__':
    """Test function for connectivity
    """

    mosaik_api.start_simulation(Collector())