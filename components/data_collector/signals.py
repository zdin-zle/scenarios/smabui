"""
A simple random simulator who can create random data streams

@author: Christian Reinhold and Henrik Wagner elenia@TUBS

"""


# import packages
import mosaik_api, random

META = {
    'type': 'time-based',
    'models': {
        'Generator': {
            'public': True,
            'any_inputs': True,
            'params': ['signals'],
            'attrs': ['signal_1','signal_2','signal_3','signal_4','signal_5'],
        },
    },
}

class Sim(mosaik_api.Simulator):
    """Simulator class for mosaik-signal model

    Args:
        mosaik_api (module): defines communication between mosaik and simulator

    Raises:
        ValueError: Unknown output attribute, when not described in META of simulator
    """

    def __init__(self):
        """Constructor
        """

        super().__init__(META)
        self.eid = None 

        self.generator = {}
        self.data = {}               

    def init(self, sid, time_resolution):
        """Initializer

        Args:
            sid (str): Id of the created instance of the load simulator
            time_resolution (float): Time resolution of current mosaik scenario

         Returns:
             meta: meta describing the simulator 
        """

        return self.meta

    def create(self, num, model, signals):
        """Creates instances of the mosaik-signal model

        Args:
            num (int): Number of signal models to be created (cannot be more than 1)
            model_type (str): Description of the created mosaik-signal instance
            signals (list): List (length=num) with initial values for each signal model

        Raises:
            RuntimeError: Can only create one instance of Generator

        Returns:
            List: return created entity
        """

        if num > 1 or self.eid is not None:
            raise RuntimeError('Can only create one instance of Generator.')

        # create generators for signals
        for signal in signals:
            self.generator[signal] = signals[signal]
            self.data[signal] = []
        
        self.eid = 'SignalGenerator'
        return [{'eid': self.eid, 'type': model}]

    # create signals
    def step(self, time, inputs, max_advance):
        """Performs simulation step calling the mosaik-signals model

        Args:
            time (int): Current simulation time according to step size. Given by MOSAIK.
            inputs (dict): Is a dictionary, that maps entity IDs to 
            data dictionaries which map attribute names to lists of values. Given by MOSAIK.
            max_advance (int, optional): Is the simulation time until the simulator can safely advance it's internal time without causing any causality errors.

        Returns:
            int: New time stamp (time increased by step size)
        """

        # loop over all signals
        for attr, values in self.generator.items():
            if values['generator'] == 'random':
                if values['datatype'] == 'double':
                    self.data[attr] = random.uniform(values['ulim'],values['olim'])
                elif values['datatype'] == 'int':
                    self.data[attr] = random.randint(values['ulim'],values['olim'])

        # return actual time for mosaik
        return time + 1  # Step size is 1 second

    # get data of models
    def get_data(self, outputs):
        """Gets data from the models

        Args:
            outputs (dict): Dictionary with data outputs from each signal model

        Returns:
            dict: Dictionary with simulation outputs
        """

        # create output data
        data = {}

        # loop over all output values
        for eid, attrs in outputs.items():

            # set data entry for model
            data[eid] = {}

            # loop over all attributes
            for attr in attrs:
                # Get model.val or model.delta:
                data[eid][attr] = self.data[attr]

        # return data to mosaik
        return data

if __name__ == '__main__':
    """Test function for connectivity
    """
    
    mosaik_api.start_simulation(Sim())