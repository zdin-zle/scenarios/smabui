mosaik-pandapower
==============

This package contains the Adapter to connect *Pandapower* to *mosaik*.


Starting
---------
Make sure of downloading the libraries reuired to run the model


Input
---------
Grid import could happen through Json or Excel files, and the file
should exist in the working directory of Mosaik scenario


    sim_config = {
       'Grid': {
            'python': 'mosaik_pandapower.simulator:Pandapower'
            }
    }
     
    GRID_FILE = 'path to the file.json' or  'path to the file.xlsx'
    gridsim = world.start('Grid', step_size=15*60)
    grid = gridsim.Grid(gridfile=GRID_FILE).children
    
    
* In pandapower library there exist a list of standard grids that 
  could be directly imported and simulated:

    https://pandapower.readthedocs.io/en/v2.1.0/networks.html


  
  
The following list of grid clusters that could be simulated by 

this model are:

* Simbench :


          GRID_FILE = 'name of the simbench grid'
          
* Cigre networks :


          GRID_FILE = 'cigre_(voltage level: hv,mv,lv)'
          
* Cigre network with DER :


          GRID_FILE = 'cigre_mv_all' or 'cigre_mv_pv_wind'
          
* Power system Cases :
  these networks exist as Json files in
  ~/pandapower/networks/power_system_test_case_jsons

  these files should be copied in the working directory and 
  imported as json file
          
          
Examples
---------
There are two scenarios (0,1) implementing simbench grid. The results are stored
in HDF5 database which is supported by Mosaik. You can preview the results of 
the simulation by HDFview, additionally other simulators exist, in order to run
a viable simulation scenario example (e.g. PV system, Wind)
          
Hint:
---------
* In scenario 1

The signing on 'Sgen', 'Ext_grid' elements (based on the generator viewpoint):
positive p_mw means power generation
negative p_mw means power consumption

For all other bus elements such as 'Bus', 'Load' (based on the consumer viewpoint):
positive p_mw means power consumption
negative p_mw means power generation