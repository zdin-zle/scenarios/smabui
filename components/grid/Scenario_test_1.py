#!/usr/bin/env python
# coding: utf-8

'''This Scenarion contains a grid simulated in pandapower with a wind generator simulator that simulates two turbines
the pv system connected to the grid is realised via a pv simlator tat takes its input from a csv file that contains DNI
data'''

# Import packages needed for the scenario.
import mosaik
from mosaik.util import connect_randomly, connect_many_to_one

# Specify simulator configurations
sim_config = {
    'Grid': {
        'python': 'mosaik_pandapower.simulator:Pandapower'
    },
    'CSV': {
        'python': 'simulators.csv_sim:CSV'
    },
    'DB': {
        'cmd': 'mosaik-hdf5 %(addr)s'
    },
    'WebVis': {
        'cmd': 'mosaik-web -s 0.0.0.0:8000 %(addr)s'
    },
    'PV': {
        'python': 'simulators.pv_simulator:PvAdapter'
    },
    'Ctrl': {
        'python': 'simulators.pv_controller:Controller'
    },
    'Wind': {
        'python': 'simulators.Wind_sim:WindModel'
    }
}

# Add PV data file
END = 1 * 24 * 60 * 60 # one day in seconds
START = '2014-01-01 01:00:00'
GRID_FILE = 'data/1-LV-rural1--0-sw.json'
PV_DATA = 'data/solar_data.txt' # .csv in your setup

# Set up the "world" of the scenario
world = mosaik.World(sim_config)

# Initialize the simulators
gridsim = world.start('Grid', step_size=15*60, mode='pf') # mode = 'pf' or 'pf_timeseries'
DNIdata = world.start('CSV', sim_start=START, datafile=PV_DATA)
pvsim = world.start('PV', start_date=START, step_size=15*60)
ctrlsim = world.start('Ctrl', step_size=60*60)
windsim = world.start('Wind', step_size=60*60)

# Instantiate model entities
grid = gridsim.Grid(gridfile=GRID_FILE).children
wind = windsim.WindModel.create(2)
pvdata = DNIdata.Sun.create(1)
pvs = pvsim.PV(lat=53.07)  #.create(1, lat=53.07) #  'bremen latitude'
ctrls = ctrlsim.Ctrl(target_attr='P_gen')  #.create(1, target_attr='P')

nodes_load = [element for element in grid if 'ext_load' in element.eid]
nodes_gen = [element for element in grid if 'ext_gen' in element.eid]

# Connect model entities
gens_wind = [('P_gen', 'p_mw'),
             ('P_gen', 'p_mw')]

gens_pv = [('P_gen', 'p_mw')]

world.connect(pvdata[0], pvs, 'DNI')         #connect_randomly(world, pvdata, pvs, 'DNI')
world.connect(pvs, nodes_gen[2], ('P_gen', 'p_mw'))
world.connect(wind[0], nodes_gen[7], gens_wind[0])
#world.connect (wind[1], nodes_gen[5], gens_wind[1])
#for i in range(len(pvs)):
world.connect(pvs, ctrls, ('P_gen', 'P'), async_requests=True) # Cyclic data flow!

#Save data in Scenrio file
db = world.start('DB', step_size=15*60, duration=END)
hdf5 = db.Database(filename='Scenario_test_1_results.hdf5')
#connect_many_to_one(world, wind, hdf5, 'P_gen')
nodes = [e for e in grid if e.type in 'Bus']
connect_many_to_one(world, nodes, hdf5, 'p_mw', 'q_mvar', 'vm_pu', 'va_degree')

#run
world.run(until=END)
