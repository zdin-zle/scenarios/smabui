#!/usr/bin/env python
# coding: utf-8
# Import packages needed for the scenario
import mosaik
from mosaik.util import connect_randomly, connect_many_to_one



sim_config = {
    'Grid': {
         'python': 'mosaik_pandapower.simulator:Pandapower'
    },
    'DB': {
         'cmd': 'mosaik-hdf5 %(addr)s'
    }
}



# Add PV data file
END = 2 * 24 * 60 * 60  #two days
START = '2014-01-01 01:00:00'
GRID_FILE = '1-LV-rural3--0-sw' # import directly from pandpaower simbench module



# Set up the "world" of the scenario.
world = mosaik.World(sim_config)



# Initialize the simulators.
gridsim = world.start('Grid', step_size=15*60, mode = 'pf_timeseries') # mode = 'pf' or 'pf_timeseries'



# Instantiate model entities.
grid = gridsim.Grid(gridfile=GRID_FILE).children



#Save results
db = world.start('DB', step_size=15*60, duration=END)
hdf5 = db.Database(filename='Scenario_test_0_results.hdf5')
nodes = [e for e in grid if e.type in 'Bus']
connect_many_to_one(world, nodes, hdf5, 'p_mw', 'q_mvar', 'vm_pu', 'va_degree')



#Run
world.run(until=END)


