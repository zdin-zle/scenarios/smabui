# -*- coding: utf-8 -*-
"""
Created on Wed Nov  7 09:10:21 2018

@author: relshinawy
"""

# Wind_sim.py
""" Mosaik interface for the example wind"""
import mosaik_api

import simulators.Wind_2 as Wind


META = {
    'models': {
        'WindModel': {
            'public': True,
            'any_inputs': True,
            'params': [],
            'attrs': ['P_gen']
        },
    },
}


class WindModel(mosaik_api.Simulator):
    def __init__(self):
        super().__init__(META)
        self.simulator = Wind.Simulator()
        self.results = []
        self.entities = {}  # Maps EIDs to model indices in self.simulator
        self.step_size = None #86400
        self.eid_prefix = 'WindModel_'
        self.cache = []
        self.increment = 0

    def init(self, sid, step_size):
        self.step_size = step_size
        return self.meta

    def create(self, num, model):
      next_eid = len(self.entities)
      entities = []

      for i in range(next_eid, next_eid + num):
          eid = '%s%d' % (self.eid_prefix, i)
          self.entities[eid] = i
          entities.append({'eid': eid, 'type': model})

      self.results = self.simulator.start() # Start the simulation one time
      return entities

    def step(self, time, inputs):
        print(time)
        self.cache = self.results[self.increment] / 1000000     #scaling for pandapower
        self.increment += 1
        return time + self.step_size

    def get_data(self, outputs):
        print(outputs)
        data = {}
        
        for eid, attrs in outputs.items():
            model_idx = self.entities[eid]
            data[eid] = {}
            for attr in attrs:
                data[eid][attr] = self.cache
        print(data)        
        return data


def main():
    return mosaik_api.start_simulation(WindModel(), 'mosaik-wind simulator')


if __name__ == '__main__':
    main()