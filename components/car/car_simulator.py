"""
Mosaik interface for the charging station simulator.

@author: Christian Reinhold and Henrik Wagner elenia@TUBS

"""


import mosaik_api
from components.car import car

# SIMULATION META DATA
META = {
    'type': 'time-based',
    'models': {
        'car': {
            'public': True,
            'params': ['init_vals'],
            'attrs': ['appearance', 'P_CHARGE_MAX', 'P_DISCHARGE_MAX', 'E_BAT_MAX', 'E_BAT', 'P_BAT', 'SOC',
            'STANDING_TIME', 'STANDING_TIME_START', 'STANDING_TIME_END', 'BEV_consumption_period', 'SIGNAL_PROGNOSIS_EV_CS',
            'TIME','ONE_MIN_PICKLE','NEXT_STEP_STANDING_MINUTES','NEXT_STEP_MINUTES'],
        },
    },
    'extra_methods': ['get_entities']
}

# ------------INPUT-SIGNALS--------------------
# appearance            Appearance of car at charging station (1: yes, 0: no)

# ------------OUTPUT-SIGNALS--------------------
# P                     Charging/Discharging active power [W]

class Sim(mosaik_api.Simulator):
    """Simulator class for mosaik-car model

    Args:
        mosaik_api (module): defines communication between mosaik and simulator

    Raises:
        ValueError: Unknown output attribute, when not described in META of simulator
    """

    # contructor
    def __init__(self):
        """Constructor
        """

        super(Sim, self).__init__(META)

        # assign properties 
        self.step_size = None

        # create dict for entities
        self.entities = {}

        # init Simulator of models
        self.simulator = car.Simulator()        

    # init API
    def init(self, sid, time_resolution, step_size = 1):
        """Initializer

        Args:
            sid (str): Id of the created instance of the load simulator (e.g. CarSim-0).
            time_resolution (float): Time resolution of current mosaik scenario.
            step_size (int, optional): Simulation step size.

         Returns:
             meta: meta describing the simulator 
        """

         # assign properties
        self.sid = sid
        self.step_size = step_size

        # return meta data
        return self.meta

    # create model
    def create(self, num, model_type, init_vals):
        """Creates instances of the mosaik-car model

        Args:
            num (int): Number of car models to be created
            model_type (str): Description of the created mosaik-car instance (here "Car")
            init_vals (list): List (length=num) with initial values for each car model

        Returns:
            dict: return created entities
        """

        # get next id
        next_eid = len(self.entities)

        # create list for entities
        entities = []

        # loop over all entities
        for i in range(next_eid, next_eid + num):

            # create name of model
            ename = '%s%s%d' % (model_type, '_', i)

            # append step size
            init_vals[i]["delta"] = self.step_size

            # create new model with init values
            model = self.simulator.add_model(init_vals[i])
            
            # add model information
            self.entities[ename] = {'ename': ename, 'etype': model_type, 'model': model}

            # append entity to list
            entities.append({'eid': ename, 'type': model_type})

        # return created entities
        return entities

    # perform simulation step
    def step(self, time, inputs, max_advance):
        """Performs simulation step calling the mosaik-car model

        Args:
            time (int): Current simulation time according to step size. Given by MOSAIK.
            inputs (dict): Is a dictionary, that maps entity IDs to 
            data dictionaries which map attribute names to lists of values. Given by MOSAIK.
            max_advance (int, optional): Is the simulation time until the simulator can safely advance 
            it's internal time without causing any causality errors.

        Returns:
            int: New time stamp (time increased by step size)
        """
        
        # loop over input signals
        for eid, attrs in inputs.items():
            for attr, values in attrs.items():
                # set value
                setattr(self.entities[eid]["model"],attr,sum(values.values()))


        # perform simulation step
        self.simulator.step(time)

        # return actual time for mosaik
        return time + self.step_size

    # get data of models
    def get_data(self, outputs):
        """Gets the data for the next concatenated model

        Args:
            outputs (dict): Dictionary with data outputs from each car model

        Raises:
            ValueError: Error if attribute not in model metadata

        Returns:
            dict: Dictionary with simulation outputs
        """

        # create output data
        data = {}

        # loop over all output values
        for eid, attrs in outputs.items():

            # set data entry for model
            data[eid] = {}

            # loop over all attributes
            for attr in attrs:
                if attr not in self.meta['models'][self.entities[eid]["model"].type]['attrs']:
                    raise ValueError('Unknown output attribute: %s' % attr)

                # Get model.val or model.delta:
                data[eid][attr] = getattr(self.entities[eid]["model"], attr)

        # return data to mosaik
        return data

    # get entities
    def get_entities(self):
        """Provides a list with description of the entities

        Returns:
            list: List with description of the entities
        """

        # return entities of API
        return self.entities


def main():
    """Test function for connectivity

    Returns:
        sim: A MOSAIK simulation 
    """

    return mosaik_api.start_simulation(Sim())

if __name__ == '__main__':
    main()