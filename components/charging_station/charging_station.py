""" 
@author: Christian Reinhold and Henrik Wagner elenia@TUBS

The mosaik-cs (charging station) model is built to manage the charging processes
of EVs considering four different charging strategies:
default respectively maximum power charging, forecast-based
charging, solar charging, and night charging. The mosaik-car model
is therefore connected to mosaik-cs.

"""


from sqlite3 import Time
from typing import Counter

class charging_station:
    """Models a EV charging station based on different charging stations
    """

    # static properties
    type = 'charging_station'   # model type [-]
    output_type = 'AC'          # source AC/DC [-]
    P_rated = 11000             # rated active power (AC/DC) [W]
    eff_dis = .99               # discharging efficiency [-]
    eff_charge = .99            # charging efficiency [-]
    strategy = 'max_P'          # charging strategy [-]
    
    # dynamic input properties
    # car signals
    appearance = None                   # Identifier if car at station [-]
    E_BAT = None                        # Actual charging level [kWh]
    E_BAT_MAX = None                    # Maximal capacity [kWh]
    P_CHARGE_MAX = None                 # Maximal charging active power [W]
    P_DISCHARGE_MAX = None              # Maximal discharging active power [W]
    STANDING_TIME = None                # Standing time for prognosis-based charging at HOME [s]
    STANDING_TIME_START = None          # Start point of standing time [s]
    STANDING_TIME_END = None            # End point of standing time [s]
    TIME = None                         # Current simulation time, not step [s]
    NEXT_STEP_STANDING_MINUTES = None   # Standing time car at home [min]
    NEXT_STEP_MINUTES = None            # Total minutes of next step [min]
    ONE_MIN_PICKLE = None               # Selection parameter for pickle file [default: false = 15 min] [tru = 1 min]
    SIGNAL_PROGNOSIS_EV_CS = None       # Signal from car to request new forecast period based on BEV_consumption_period [-]
    BEV_consumption_period = None       # Electricity consumption of EV in next period not being home, 
                                        # starting right after STANDING_TIME_END
    PROGNOSIS_RESIDUAL = None           # residual load prognosis for charging strategies received from BEM [Wh]    
    
    # control signals & charging strategy values
    P_SET = None                        # Set-Point [W]
    RES_SIGNAL = False                  # Surplus of renewable energy [-]
    P_SALDO_RES = None                  # Power saldo of renewable energies [W]
    step_count = 0                      # Internal step count for night charging [-]
    car_night_availablabilty = False    # Availability of car overnight [-]

    # dynamic output properties
    P = None                            # Active Power (after control) [W]
    Q = None                            # Reactive Power (after control) [W]
    P_MIN = None                        # Minimal active Power [W]
    P_MAX = None                        # Maximal active Power [W]
    SIGNAL_PROGNOSIS_CS_control = None  # Signal from CS to control

    # result properties
    results = ['P','Q']

    # set data identifier
    set_data = 'appearance'

    # init of values
    def __init__(self, init_vals):
        """Initializes the mosaik-cs model

        Args:
            init_vals (dict): Initial values for creation of cs model. Read from model_data_car_cs.json, including the charging strategy, rated power and type of power (AC/DC).
        """

        # assign init values
        for i, (key, value) in enumerate(init_vals.items()):
            setattr(self, key, value)

        # edit efficiency rates
        if self.output_type == 'AC':
            self.eff_charge = 1
            self.eff_dis = 1
    
    # perform simulation step
    def step(self, time):
        """Calculate cs parameters for current simulation step 
        according to charging strategy.

        Args:
            time (int): Current simulation time
        """

        # signal transfer from charging station to control
        if self.SIGNAL_PROGNOSIS_EV_CS is not None:
            self.SIGNAL_PROGNOSIS_CS_control = float(self.SIGNAL_PROGNOSIS_EV_CS)

        # store time in self
        self.time = time 
        # execute local charging strategy
        if self.strategy == 'max_P':
        # maximal power use
            self.strategy_P_max()
        # execute prognosis-based charging strategy
        if self.strategy == 'prognosis':
            # minimal power to fully charge over standing time
            self.strategy_prognosis()
        # execute night time charging strategy
        if self.strategy == 'night_charging':
            self.strategy_night_charging()
        # execute solar charging
        if self.strategy == 'solar_charging':
            # use available surplus from PV system to use to charge the vehicle during standing time
            self.strategy_solar_charging()      

    def strategy_night_charging(self): 
        """The night charging strategy extends the charging process
            similar to forecast-based charging. The strategy is solely activated
            and processed overnight between 20:00 and 05:00 when
            grid load is mostly low or lower in residential areas. Therefore,
            the times with low grid load are matched with power-intensive
            charging processes. When the EV is not available overnight,
            maximum power charging is instead activated.
        """

        self.step_count = self.step_count + 1
        # Calculation of total night time. (Since the total night time can be calculated at once we do not want to compute the total night time in each simulation step)
        if self.TIME<=self.STANDING_TIME_END and self.step_count==1:
            self.available_night_time()
        elif self.TIME>self.STANDING_TIME_END:
            self.step_count=0
        # maximal power to fully charge at night time and use minimal fixed power to charge at other time
        if self.car_night_availablabilty == True:
            self.night_charger()
        else:
            self.strategy_P_max()

    # charging strategy with maximal power of charging station
    def strategy_P_max(self):
        """Strategy for charging the EV with the rated power (11 kW) 
        of the charging station as soon as it arrives at the home location.
        """

        #strategy for default pickle file (15 min)
        if self.ONE_MIN_PICKLE == False:      
        # check if car at station
            if self.appearance is None or self.appearance == 0:
            # set P/Q to zero
                self.P_MIN = 0
                self.P_MAX = 0
            elif self.appearance == 1:
            # calculation of active power
                self.P_MIN = 0
                self.P_MAX = min(self.P_CHARGE_MAX/self.eff_charge,self.P_rated)
        
        #strategy for 1 min pickle file   
        elif self.ONE_MIN_PICKLE == True:
            if self.appearance is None or self.appearance == 0:
            # set P/Q to zero
                self.P_MIN = 0
                self.P_MAX = 0

            elif self.appearance == 1:
            # calculation of active power in dependence of standing time 
                self.P_MIN = 0
                self.P_MAX = min(self.P_CHARGE_MAX/self.eff_charge,self.P_rated) * (self.NEXT_STEP_STANDING_MINUTES/self.NEXT_STEP_MINUTES)
    
        # check set point
        if self.P_SET is None:
            # use maximal power
            self.P = self.P_MAX
        else:
            # use set point if possible
            self.P = min(self.P_MAX,self.P_SET)

        # calculate Q-value
        self.Q = 0
    
    # charging strategy with minimal power over prognosed standing_time to fully charge EV
    def strategy_prognosis(self):
        """The forecast-based charging strategy
            estimates the total standing time of the connected EV at
            home and then calculates the minimal charging power needed
            to fully charge the EV. As this spreads the charging process,
            the strategy dramatically reduces the needed level of charging
            power following a lower grid electricity demand per timestep.
        """

        # check if car at station
        if self.appearance is None or self.appearance == 0:
            # set P/Q to zero
            self.P_MIN = 0
            self.P_MAX = 0
        elif self.appearance == 1:
            # calculation of active power
            self.P_MIN = 0
            if self.STANDING_TIME == 0:
                self.P_MAX = min(self.P_CHARGE_MAX/self.eff_charge,self.P_rated)
            else:
                self.P_MAX = min(self.P_rated,((self.E_BAT_MAX-self.E_BAT)/(self.STANDING_TIME/3600)))

        # check set point
        if self.P_SET is None:
            # use maximal power
            self.P = self.P_MAX # self.P_MAX
        else:
            # use set point if possible
            self.P = min(self.P_MAX,self.P_SET)

        # calculate Q-value
        self.Q = 0

    # Function to evaluate the total time that the car is available for charging at night
    def available_night_time(self):
        """Function to determine the total time the car is for overnight charging.
        """

        calculated_time_start = 0
        calculated_time_end = 0
        self.total_night_time = 0 # total standing time at night
        self.total_day_time = 0 # total standing time during the day   
        standing_time_per_day = [] # night standing time per day throughout the simulation
        calculated_time_start = self.STANDING_TIME_START/(24*3600) # calc the start of charging process in days
        calculated_time_end = self.STANDING_TIME_END/(24*3600) # calc the end of charging process in days
        
        j, d = divmod(calculated_time_start, 1) # j is the day number (from the car data) from which the car is available for charging
        i, d = divmod(calculated_time_end, 1) # i is the day number (from the car data) until which the car is available for charging

        # This conditional statement checks and calculates the day after initial sim time when the charging begins
        if j==0:
            calculated_time_start = self.STANDING_TIME_START
        else:
            calculated_time_start = self.STANDING_TIME_START - j*24*3600

        # This conditional statement checks and calculates the day after initial sim time when the charging ends
        if i==0:
            calculated_time_end = self.STANDING_TIME_END
        else: 
            calculated_time_end = self.STANDING_TIME_END - i*24*3600
            if calculated_time_end == 0:
                calculated_time_end = 24*3600

        # This literal stores the total number of days for which the car is available for charging
        difference_days = i - j 

        """
        In the following conditional statements, we see how many overnight charging cycles are possible for the vehicle.
        This is calculated by looking at the total number of days that the car is available at home. 
        Between the start and end time we have the intermediate number of days and when the car is available for the full day, 
        we have 9 hours of charging time.
        For the rest days, we check if the car is available during our decided night time which is between 20:00h and 05:00h 
        and calculate the total number of hours that the car is available for overnight charging.
        """
        
        if difference_days != 0: 
            if difference_days>2: 
                for k in range (i+1,j):
                        standing_time_per_day.append(9*3600)
            elif calculated_time_end/3600 >= 20 :
                hours = 5*3600 + calculated_time_end - 20*3600
                standing_time_per_day.append(hours)
            elif calculated_time_end/3600 <= 5:
                hours = calculated_time_end
                standing_time_per_day.append(hours)
            else :
                hours = 5*3600
                standing_time_per_day.append(hours)
            if calculated_time_start/3600 >= 20 :
                hours = calculated_time_start - 20*3600
                standing_time_per_day.append(hours)
            elif calculated_time_start <= 5:
                hours = 4*3600 + 5*3600 - calculated_time_start
                standing_time_per_day.append(hours)
            else :
                hours = 4*3600
                standing_time_per_day.append(hours)
            # We sum up all the individual night standing times into our total available night time
            self.total_night_time = sum(standing_time_per_day) 
            self.total_day_time = self.STANDING_TIME - self.total_night_time
            self.car_night_availablabilty = True

        else:
            self.car_night_availablabilty = False
            # raise Exception('The car is not available overnight')

    # Charging strategy to charge car at optimal power when car is available to charge overnight
    def night_charger(self):
        """Function to determine the lowest possible charging power to fully charge the car overnight
        """

        # strategy for 15 min pickle file (default)
        if self.ONE_MIN_PICKLE == False: 
            # check if car at station
            if self.appearance is None or self.appearance == 0:
                # set P/Q to zero
                self.P_MIN = 0
                self.P_MAX = 0
            elif self.appearance == 1:
                # calculation of active power
                self.P_MIN = 0
                if self.STANDING_TIME == 0:
                    self.P_MAX = min(self.P_CHARGE_MAX/self.eff_charge,self.P_rated) 
                elif self.TIME<self.STANDING_TIME_END:
                    i, d = divmod(self.TIME/(24*3600), 1) # i gives the day number based on the current simulation time
                    if i == 0:
                        # no charging during daytime - currently 05:00-20:00
                        if 5*3600<self.TIME and self.TIME<20*3600:
                            self.P_MAX = 0
                        else: 
                            self.P_MAX = min(self.P_rated,((self.E_BAT_MAX-self.E_BAT)/(self.total_night_time/3600)))
                    else:
                        # i(days)*24(hours_per_day)*5(hours_day_period)*3600(timestep)
                        if ((i*24)+5)*3600<self.TIME and self.TIME<((i*24)+20)*3600:
                            self.P_MAX = 0
                        else:
                            self.P_MAX = min(self.P_rated,((self.E_BAT_MAX-self.E_BAT)/(self.total_night_time/3600)))
                else:
                    self.P_MAX=0
                    
        # strategy for 1 min pickle file   
        elif self.ONE_MIN_PICKLE == True:
            # check if car at station
            if self.appearance is None or self.appearance == 0:
                # set P/Q to zero
                self.P_MIN = 0
                self.P_MAX = 0
            elif self.appearance == 1:
                # calculation of active power
                self.P_MIN = 0
                if self.STANDING_TIME == 0:
                    self.P_MAX = min(self.P_CHARGE_MAX/self.eff_charge,self.P_rated) * (self.NEXT_STEP_STANDING_MINUTES/self.NEXT_STEP_MINUTES)
                elif self.TIME<self.STANDING_TIME_END:
                    i, d = divmod(self.TIME/(24*3600), 1) # i gives the day number based on the current simulation time
                    if i == 0:
                        # no charging during daytime - currently 05:00-20:00
                        if 5*3600<self.TIME and self.TIME<20*3600:
                            self.P_MAX = 0
                        else: 
                            self.P_MAX = min(self.P_rated,((self.E_BAT_MAX-self.E_BAT)/(self.total_night_time/3600))) * (self.NEXT_STEP_STANDING_MINUTES/self.NEXT_STEP_MINUTES)
                    else:
                        if i*5*3600<self.TIME and self.TIME<i*20*3600:
                            self.P_MAX = 0
                        else:
                            self.P_MAX = min(self.P_rated,((self.E_BAT_MAX-self.E_BAT)/(self.total_night_time/3600))) * (self.NEXT_STEP_STANDING_MINUTES/self.NEXT_STEP_MINUTES)
                else:
                    self.P_MAX=0

        # check set point
        if self.P_SET is None:
            # use maximal power
            self.P = self.P_MAX
        else:
            # use set point if possible
            self.P = min(self.P_MAX,self.P_SET)

            # calculate Q-value
            self.Q = 0 

    # Charging strategy to charge car with produced surplus renewable energy
    def strategy_solar_charging(self):
        """The solar charging strategy is initiated within times of solar energy surplus. The mosaik-cs model receives
        the information of such a surplus from mosaik-control.
        """

        # strategy for 15 min pickle file (default)
        if self.ONE_MIN_PICKLE == False: 
            # check if car at station
            if self.appearance is None or self.appearance == 0:
                # set P/Q to zero
                self.P_MIN = 0
                self.P_MAX = 0
            elif self.appearance == 1:
                # calculation of active power
                self.P_MIN = 0
                if self.STANDING_TIME == 0:
                    self.P_MAX = min(self.P_CHARGE_MAX/self.eff_charge,self.P_rated)
                else:
                    # The RES_SIGNAL informs the charger if there is available surplus energy that can be used for the charging process
                    # If available then this surplus is used to charge our EV
                    if self.RES_SIGNAL == True:
                        self.P_MAX =  min(abs(self.P_SALDO_RES)/self.eff_charge,self.P_rated) 
                    else:
                        self.P_MAX = 0
            # check set point
            if self.P_SET is None:
                # use maximal power
                self.P = self.P_MAX
            else:
                # use set point if possible
                self.P = min(self.P_MAX,self.P_SET)

        # strategy for 1 min pickle file   
        elif self.ONE_MIN_PICKLE == True:
            # check if car at station
            if self.appearance is None or self.appearance == 0:
                # set P/Q to zero
                self.P_MIN = 0
                self.P_MAX = 0
            elif self.appearance == 1:
                # calculation of active power
                self.P_MIN = 0
                if self.STANDING_TIME == 0:
                    self.P_MAX = min(self.P_CHARGE_MAX/self.eff_charge,self.P_rated) * (self.NEXT_STEP_STANDING_MINUTES/self.NEXT_STEP_MINUTES)
                else:
                    # The RES_SIGNAL informs the charger if there is available surplus energy that can be used for the charging process
                    # If available then this surplus is used to charge our EV
                    if self.RES_SIGNAL == True:
                        self.P_MAX =  min(abs(self.P_SALDO_RES)/self.eff_charge,self.P_rated) * (self.NEXT_STEP_STANDING_MINUTES/self.NEXT_STEP_MINUTES)
                    else:
                        self.P_MAX = 0
            # check set point
            if self.P_SET is None:
                # use maximal power
                self.P = self.P_MAX
            else:
                # use set point if possible
                self.P = min(self.P_MAX,self.P_SET)

        # calculate Q-value
        self.Q = 0 

class Simulator(object):
    """Simulator class for mosaik-cs model
    Creates multiple instances and saves the results in an accesible list

    Args:
        object (class): Generic class
    """

    def __init__(self):
        """Constructor
        """

        # init data elments
        self.models = {}

    # create model instances
    def add_model(self, model_name, init_vals):
        """Create model instances of mosaik-cs

        Args:
            model_name (str): name including eid of cs model (e.g. charging_station_0)
            init_vals (dict): List (length=num) with initial values for each cs model

        Returns:
            model: returns created cs object
        """

        # create model with init values
        model = charging_station(init_vals)

        # add model to model storage
        self.models[model_name] = {'name': model_name, 'type': 'charging_station','model': model, 'results': []}

        # return model
        return model

    # perform simulation step of all models
    def step(self, time):
        """Performs simulation step of all to simulator connected models

        Args:
            time (int): Current simulation time.
        """

        # Enumaration over all models in simulator
        for i, model_entry in self.models.items():
            # perform simulation step
            model_entry["model"].step(time)

            # collect data of model and storage local
            for j, signal in enumerate(model_entry["model"].results):
                model_entry["results"].append(getattr(model_entry["model"],signal)) 

    # get data for set data
    def get_set_data(self, inputs):
        """Gets determined charging power from cs model and sets
        charging power of car model accordingly

        Args:
            inputs (dict): collected input data from mosaik-car models

        Returns:
            dict: Set charging power and therefore updated P_BAT values for mosaik-car models. 
        """
        
        # create data element
        data = {}

        # loop over all models
        for eid, attrs in inputs.items():

            # get model entry
            model_entry = self.models[eid]

            # get signal partner
            signal_partners = attrs.get(model_entry["model"].set_data, {})

            # loop over all signal partners
            for model_eid, value in signal_partners.items():

                # create entry
                if eid not in data:
                    data[eid] = {}
                if eid not in data[eid]:
                    data[eid][model_eid] = {}

                # get values
                data[eid][model_eid]['P_BAT'] = model_entry["model"].P

        # return data
        return data