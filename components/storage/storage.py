"""
Model of an electrical battery storage including dynamic efficiency 
curves of battery inverter

@author: Christian Reinhold and Henrik Wagner elenia@TUBS

@review: Fernando Penaherrera @OFFIS/UOL

"""


# import packages
# import decimal
import decimal
from decimal import Decimal
decimal.getcontext().prec = 4

class eStorage:
    """electric Storage-Model
    Can be created with default values or with a dictionary of initial values.
    An energy management model (e.g. mosaik_control) can be used to determine P_SET values.

    >>> eStorageModel1 = eStorage()
    >>> eStorage2 = eStorage(init_vals={"E_bat_rated": 5000,
                                        "soc_init": 0.1,
                                        "loss_rate": 0.02,
                                        "DOD_max": 0.95})
    """

    # Static properties - overwritten by model_data.json
    type = 'eStorage'               # Model type [-]
    soc_init = 0.5                  # Initial SOC [-]
    E_bat_rated = 10000             # Capacity [Wh]
    P_rated_discharge_max = 8000    # Rated discharging active power [W]
    P_rated_charge_max = 8000       # Rated charging active power [W]
    eff_discharge_init = 0.95       # static efficiency discharge [-]
    eff_charge_init = 0.95          # static efficiency charge [-]
    status_curve = False            # on/off dynamic charging [-]
    loss_rate = 0.02                # self-discharge per month [%/month]
    DOD_max = 0.95                  # max. depth of discharge [-]
    status_aging = False            # on/off consideration of reduced battery 
                                    # capacity over time (battery aging effects)
    SoH_init = 1                    # Initial SoH at t=0, 1 equals 100%
    SoH_cycles_max = 0.8            # Cycle limit of SoH, at 80% mostly battery 
                                    # turns into 2nd life
    bat_cycles_max_soh = 0.8        
    bat_cycles_max = 5000           # Max. battery cycles [-]
    bat_cycles_init = 0             # Initial battery cycles storage [-]

    # Battery operation strategies
    operation_strategy = 'uncontrolled' # Default strategy: uncontrolled charging

    # Parameter of operation strategies
    charging_limit = 'None'            # Percentage to which battery charging power is reduced to avoid curtailment of PV
    
    # Simulation properties
    delta = 60                      # Time-Delta [s]

    # Dynamic input properties
    
    P_SET = None                    # Set-Point - Battery Active Power 
                                    # (+ Charge, - Discharge) [W]

    # Dynamic output properties
    E_BAT = None                    # Current charging level [Wh]
    P_SET_LIMIT = None              # Limitation of Set Point [W]
    SOC = None                      # Current SOC - State of Charge [-]
    P_CHARGE_MAX = None             # Current maximal active charge power [W]
    P_DISCHARGE_MAX = None          # Current maximal active discharge power [W]
    BAT_CYCLES = None               # Current amount of battery cycles [-]
    SOH = None                      # Current state of health of battery [-]
    P = None                        # Current charging power at grid node [W]

    # result properties
    results = [
        'E_BAT',
        'P_SET_LIMIT',
        'SOC',
        'P_CHARGE_MAX',
        'P_DISCHARGE_MAX',
        'BAT_CYCLES',
        'SOH']

    def __init__(self, init_vals=None):
        """Initializes the sotrage model. Uses the the static properties as entry

        Args:
            init_vals (dict, optional): Initial values for creation of storage model.
        """
        
        # assign initial values from the init_vals dictionary
        if init_vals:
            for key, value in init_vals.items():
                setattr(self, key, value)

        # set actual SOC
        self.SOC = self.soc_init

        # calculate usable battery capacity
        self.E_bat_usable = self.E_bat_rated * self.DOD_max

        # set real charging level E-Bat
        self.E_BAT = self.SOC * self.E_bat_usable

        # calculate energy of one charge-discharge cycle
        self.E_cycle = 2 * self.E_bat_usable

        # set amount of battery cycles
        self.BAT_CYCLES = self.bat_cycles_init

        # set initial SoH
        self.SOH = self.SoH_init

        # set initial set point for active battery power value
        self.P_SET = 0

        # set initial charge/discharge efficiency
        self.eta_charge = self.eff_charge_init
        self.eta_discharge = self.eff_discharge_init

        # calculate self-discharge per timestep
        self.self_discharge = (
            self.loss_rate / (30 * 24 * 60 * 60)) * self.delta

    def set_active_power_limit(self):
        """Sets the output active power.
        Controls that it does not exceed the battery charge
        and discharge limits
        """

        if self.operation_strategy == 'uncontrolled':
            self.uncontrolled()
        
        elif self.operation_strategy == 'minimize_curtailment':
            self.minimize_curtailment()
    
    def minimize_curtailment(self):
        """This strategy tries to minimize the curtailment of the solar power system
        by reducing the charging power. This decreases self-sufficiency but also the
        curtailment due to EEG2021 (e.g. feed-in is only allowed at max. 70% of 
        installed power for uncontrolled solar power systems). This operation strategy
        should not be used in combination with electric vehicle charging. Suitable limits
        are 30%-60%.
        """

        # check if charging limit is set
        if self.charging_limit == None:
            # set default limit of 60%
            self.charging_limit = 0.6
        # Set Limit considering charging limits to reduce curtailment
        if self.P_SET > (self.P_rated_charge_max*self.charging_limit):
            self.P_SET_LIMIT = self.P_rated_charge_max*self.charging_limit
        elif self.P_SET < -self.P_rated_discharge_max:
            self.P_SET_LIMIT = -self.P_rated_discharge_max
        else:
            self.P_SET_LIMIT = self.P_SET
       
    def uncontrolled(self):
        """Standard operation strategy for battery storage system.
        """

        # Set Limit
        if self.P_SET > self.P_rated_charge_max:
            self.P_SET_LIMIT = self.P_rated_charge_max
        elif self.P_SET < -self.P_rated_discharge_max:
            self.P_SET_LIMIT = -self.P_rated_discharge_max
        else:
            self.P_SET_LIMIT = self.P_SET

    def calculate_new_charging_level(self,time):
        """Calculates the updated energy content after charging/discharging
        Uses charging efficiency levels (eta_charge / eta_discharge)
        """

        # reset turnover/volume of energy per step
        self.E_bat_step_volume = 0
        
        # Calculate new charging level
        
        # Charging
        if self.P_SET_LIMIT >= 0:  
            self.calc_eta_charge()
            overcharging_signal = False

            # calculate new energy volume of storage
            if self.E_BAT < self.E_bat_usable:
                self.E_bat_step_volume = self.P_SET_LIMIT * \
                    (self.delta / 3600) * self.eta_charge
                self.E_BAT = self.E_BAT + self.E_bat_step_volume

                # Check that battery energy content is not surpassed
                if self.E_BAT > self.E_bat_usable:
                    overcharging_signal = True
                    E_overcharged = self.E_BAT - self.E_bat_usable
                    self.E_BAT = self.E_bat_usable
                    
            # set power value of storage for grid calculation
            if self.E_bat_step_volume == 0:
                self.P = 0
            elif overcharging_signal == True:
                self.P = (self.E_bat_step_volume - E_overcharged) \
                        / self.eta_discharge / (self.delta/3600)
            else:
                self.P = self.P_SET_LIMIT
        
        # Discharging
        elif self.P_SET_LIMIT < 0:  
            self.calc_eta_discharge()  # calculate dynamic discharging efficiency
            undercharging_signal = False
            
            if self.E_BAT > 0:
                self.E_bat_step_volume = self.P_SET_LIMIT * \
                    self.delta / (3600 * self.eta_discharge)
                self.E_BAT = self.E_BAT + self.E_bat_step_volume

                # Check if battery level drops under 0
                if self.E_BAT < 0:
                    E_undercharged = abs(self.E_BAT)
                    self.E_BAT = 0 
            
            # set power value of storage for grid calculation
            if self.E_BAT == 0:
                self.P = 0
            elif self.E_bat_step_volume == 0:
                self.P = 0
            elif undercharging_signal == True:
                self.P = (self.E_bat_step_volume - E_undercharged) \
                        / self.eta_discharge / (self.delta/3600)
            else:
                self.P = self.P_SET_LIMIT


    def step(self, time):
        """Advances the battery model.
        Calculates all of the Dynamic Properties based on the Static Properties.

        
        Args:
            time (int): Current simulation time
        """
        
        # Set active power limit
        self.set_active_power_limit()

        # Calculate energy loss due to self-discharge
        self.E_BAT = self.E_BAT * (1 - self.self_discharge)

        # Calculate resulting power flow at grid node P and new energy level E_BAT
        self.calculate_new_charging_level(time)

        # Calculate SOC
        self.SOC = min(self.E_BAT / self.E_bat_usable, 1)

        # Calculate battery cycles
        self.BAT_CYCLES = self.BAT_CYCLES + \
            abs(self.E_bat_step_volume / self.E_cycle)

        # Calculate maximum discharging active power
        """Note that P_DISCHARGE_MAX cannot be 0 as control then never initiates a charging/discharging process.
        Therefore, in times with low P_SET we set P_Discharge higher to show the potential"""
        if self.eta_discharge == 0 and self.P_ratio == 0:
            self.P_DISCHARGE_MAX = min((self.E_bat_usable - self.E_BAT) / self.eta_discharge_max / (self.delta / 3600),
                    self.P_rated_discharge_max) 
        
        else:
            self.P_DISCHARGE_MAX = min(
                self.E_BAT * self.eta_discharge / (self.delta / 3600),
                self.P_rated_discharge_max)

        # Calculate maximum charging active power
        """Note that P_CHARGE_MAX cannot be 0 as control then never initiates a charging/discharging process
        Therefore, in times with low P_SET we set P_Discharge higher to show the potential"""
        #TODO: check if round(self.P_ratio,3) == 0 makes sense
        if (self.eta_charge == 0 and self.P_ratio == 0) or (self.eta_charge == 0 and self.P_SET_LIMIT <= 0):
            self.P_CHARGE_MAX = min((self.E_bat_usable - self.E_BAT) / self.eta_charge_max / (self.delta / 3600),
                    self.P_rated_charge_max) 
        
        else:
            if self.operation_strategy == 'minimize_curtailment':
                self.P_CHARGE_MAX = min(
                    (self.E_bat_usable - self.E_BAT) / self.eta_charge / (self.delta / 3600),
                    self.P_rated_charge_max*self.charging_limit)
            else:
                try:
                    self.P_CHARGE_MAX = min(
                        (self.E_bat_usable - self.E_BAT) / self.eta_charge / (self.delta / 3600),
                        self.P_rated_charge_max)
                except:
                    self.P_CHARGE_MAX = 0

        # Calculate battery state of health and aging properties
        if self.status_aging:
            self.calculate_aging_status()

    def calculate_aging_status(self):
        """Checks if the State of Health (SoH) limit has been reached.

        Raises:
            ValueError: Warning: Minimum SoH of %s has been reached - battery needs to be replaced or SoH limit bat_cycles_init to be increased
        """
        
        # Check if SoH limit is already reached
        if self.SOH > (1 - self.SoH_cycles_max):
            
            # Calculate bat_aging per cycle and new SoH
            self.bat_aging = (self.SoH_cycles_max - self.SoH_init) / \
                (self.bat_cycles_max - self.BAT_CYCLES)
            self.SOH = self.SoH_init + self.bat_aging * self.BAT_CYCLES
            
            # calculate reduced E_bat_usable due to aging
            self.E_bat_usable = (
                self.E_bat_rated * self.DOD_max) * self.SOH
        
        elif self.SoH <= (1 - self.SoH_cycles_max):
            raise ValueError(
                "Warning: Minimum SoH of %s has been reached - battery needs to be replaced or SoH limit / bat_cycles_init to be increased" % (1 - self.SoH_cycles_max))

    def calc_eta_discharge(self):
        """Calculation of dynamic discharging efficiency (eta_BAT2AC)
        """
    
        if self.status_curve:
            self.eta_BAT2AC = [Decimal(a) for a in self.eta_BAT2AC]
            
            self.P_ratio = Decimal(
                abs(self.P_SET_LIMIT / self.P_rated_discharge_max))
            
            if self.P_ratio <= Decimal(0.012): # note: battery inverter does not start for low P_ratio
                self.eta_discharge = 0 
            else:
                # Formula:
                # eta = a0 * (P/P_max)^a1 + a2
                self.eta_discharge = float(self.eta_BAT2AC[0] * (
                    self.P_ratio**self.eta_BAT2AC[1])+ self.eta_BAT2AC[2])
            
        # calc eta_discharge_max = eta_discharge for P_rated_discharge_max
        self.eta_discharge_max = float(self.eta_BAT2AC[0] * (
           1**self.eta_BAT2AC[1])+ self.eta_BAT2AC[2])

    def calc_eta_charge(self):
        """Calculation of dynamic charging efficiency (eta_AC2BAT)        
        """

        if self.status_curve:
            self.eta_AC2BAT = [Decimal(a) for a in self.eta_AC2BAT]

            self.P_ratio = decimal.Decimal(
                abs(self.P_SET_LIMIT / self.P_rated_charge_max))
            if self.P_ratio <= Decimal(0.012):  # note: battery inverter does not start for low P_ratio
                self.eta_charge = 0
            else:
                # Formula:
                # eta = a0 * (P/P_max)^a1 + a2
                self.eta_charge = float(self.eta_AC2BAT[0] * (
                    self.P_ratio**self.eta_AC2BAT[1]) + self.eta_AC2BAT[2])
        # calc eta_discharge_max = eta_discharge for P_rated_discharge_max
        self.eta_charge_max = float(self.eta_BAT2AC[0] * (
           1**self.eta_BAT2AC[1])+ self.eta_BAT2AC[2])
    
    def __repr__(self):
        """String for identification of the battery

        Returns:
            str: identification of the battery
        """
        
        text = "Electric Battery Storage Model" + "\n"
        text += "Capacity [Wh] = %s" % (self.E_bat_rated) + "\n"
        text += "Real Capacity [Wh] = %s" % (self.E_bat_usable) + "\n"
        text += "Maximum Charging Power [W] = %s" % (
            self.P_CHARGE_MAX) + "\n"
        text += "Maximum Discharging Power [W] = %s" % (self.P_DISCHARGE_MAX) + "\n"
        return text 

class hStorage:
    """Hydrogen storage model
    """

    # static properties
    type = 'hStorage'

    # constructor
    def __init__(self, init_vals):
        """Initializes the mosaik-storage model

        Args:
            init_vals (dict): Initial values for creation of a hydrogen storage model.
        """

        # assign init values
        for _, (key, value) in enumerate(init_vals.items()):
            setattr(self, key, value)


class Simulator(object):
    """Contains a list with storage models and a result list
    Used to access information on the different entities of the model

    Args:
        object (class): Generic class
    """
    
    # Constructor
    def __init__(self):
        """Constructor
        """

        # init data elements
        self.models = []
        self.results = []

    # create model instances
    def add_model(self, etype='eStorage', init_vals=None):
        """Create model instances of mosaik-storage

        Args:
            etype (str): name of load model (default to 'eStorage')
            init_vals (dict): List (length=num) with initial values for each storage model

        Returns:
            model: returns created storage object
        """

        # select by type
        if etype == 'eStorage':
            # create electric storage model
            model = eStorage(init_vals)
        elif etype == 'hStorage':
            # create hydrogen storage model
            model = hStorage(init_vals)

        # add model to model storage
        self.models.append(model)

        # add list for simulation data
        self.results.append([])

        # return model
        return model

    # perform simulation step of all models
    def step(self,time):
        """Performs simulation step of all to simulator connected models

        Args:
            time (int): Current simulation time.
        """
        
        # Enumeration over all models in simulator
        for i, model in enumerate(self.models):
            # perform simulation step
            model.step(time)

            # collect data of model and storage local
            for j, signal in enumerate(model.results):
                self.results[i].append(getattr(model, signal))