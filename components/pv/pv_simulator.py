"""
Mosaik interface for the pv simulator.

@author: Christian Reinhold and Henrik Wagner elenia@TUBS

"""


import mosaik_api
import arrow
from components.pv import pv

META = {
    'type': 'time-based',
    'models': {
        'PV': {
            'public': True,
            'params': ['init_vals'],
            'attrs': ['P_ACTUAL', 'P_MIN', 'P_MAX', 'P', 'Q', 'P_SET'],
        }
    },
    "extra_methods": ["get_entities"]
}

class Sim(mosaik_api.Simulator):
    """Simulator class for mosaik-pv model

    Args:
        mosaik_api (module): defines communication between mosaik and simulator
    """

    def __init__(self):
        """Constructor
        """

        super(Sim, self).__init__(META)
        # assign properties 
        self.step_size = None
        self.eid_prefix = 'BEM_'
        # create dict for entities
        self.entities = {}

        # init Simulator of models
        self.simulator = pv.Simulator()  

    # init mosaik API
    def init(self, sid, time_resolution, step_size = 900):
        """Initializer for PV:Sim class

        Args:
            sid (class): Id of the created instance of the pv simulator (e.g. PV-0)
            time_resolution (int, optional): Time resolution of current mosaik scenario. 
            step_size (int, optional): Step size of the simulation. Defaults to 900.

        Returns:
            meta: meta describing the simulator
        """

        # assign properties
        self.sid = sid
        self.step_size = step_size

        # return meta data
        return self.meta

    # create models
    def create(self, num, model_type, init_vals):
        """Creates instances of the mosaik-pv model

        Args:
            num (int): Number of control models to created
            model_type (str): Description of the created mosaik-pv instance (here "pv")
            init_vals (list): List (length=num) with initial values for each pv model

        Returns:
            dict: return created entities
        """

        # next entity ID
        next_eid = len(self.entities)

        # create list for entities
        entities = []

        # loop over all entities
        for i in range(next_eid, next_eid + num):
            # create name of model
            ename = '%s%s%d' % (model_type, '_', i)

            # create new model with init values
            model = self.simulator.add_model(init_vals[i])

            # create full id
            full_id = self.sid + '.' + ename

            # add model information
            self.entities[ename] = {'ename': ename, 'etype': model_type, 'model': model, 'full_id': full_id}

            # append entity to list
            entities.append({'eid': ename, 'type': model_type})

        # return created entities
        return entities

    def step(self, time, inputs, max_advance):
        """Performs simulation step calling the mosaik-pv model

        Args:
            time (int): Current simulation time according to step size. Given by MOSAIK.
            inputs (dict): Is a dictionary, that maps entity IDs to 
            data dictionaries which map attribute names to lists of values. Given by MOSAIK.
            max_advance (int, optional): Is the simulation time until the simulator can safely advance it's internal time without causing any causality errors.

        Returns:
            int: New time stamp (time increased by step size)
        """

        # loop over input signals
        for eid, attrs in inputs.items():
            for attr, values in attrs.items():
                # set value
                setattr(self.entities[eid]["model"],attr,sum(values.values()))
                
        # Perform simulation step
        self.simulator.step(time)

        # Next timestamp for simulation
        return time + self.step_size

    # return values of model
    def get_data(self, outputs):
        """Gets the data for the next concatenated model

        Args:
            outputs (dict): Dictionary with data outputs from each pv model

        Raises:
            ValueError: Error if attribute not in model metadata

        Returns:
            dict: Dictionary with simulation outputs
        """

        # create output data
        data = {}

        # loop over all output values
        for ename, attrs in outputs.items():
            # get model entry
            entry = self.entities[ename]

            # set data entry for model
            data[ename] = {}

            # loop over all attributes
            for attr in attrs:
                if attr not in self.meta['models'][entry["model"].type]['attrs']:
                    raise ValueError('Unknown output attribute: %s' % attr)

                # Get model data
                data[ename][attr] = getattr(entry["model"], attr)

        # return data to mosaik
        return data

    # get entities
    def get_entities(self):
        """Provides a list with description of the entities

        Returns:
            list: List with description of the entities
        """

        # return entities of API
        return self.entities

def main():
    """Test function for connectivity

    Returns:
        sim: A MOSAIK simulation 
    """
    
    return mosaik_api.start_simulation(Sim())