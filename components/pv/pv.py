"""
@author: Henrik Wagner and Daniel Swientek elenia@TUBS

The mosaik-pv component models the charging behavior of PV. 

"""


import random, os, arrow

class PV:
    """PV-Model
    """

    # static properties
    type = 'PV'                             # model type
    P_rated = 10000                         # rated active power [W]

    # electric properties
    cos_phi = 1                             # cos phi [-]

    # static properties - file
    P_rated_profile = 10000                 # rated active power profile
    datafile = None                         # relative path of datafile    
    date_format = 'YYYY-MM-DD HH:mm:ss'     # format of time vector 
    delimiter = ','                         # delimiter
    header_rows = 2                         # header rows
    start_date = "2014-01-01 00:00:00"      # start time in time vector
    time_resolution = 1                    # time resolution per step

    # local properties
    data = None                             # data of file [-]
    row_data = None                         # row data [-]  

    # dynamic output properties
    P_SET = None                            # Set-Point [W]

    # dynamic output properties    
    P_ACTUAL = None                         # Actual active Power (pre control) [W]
    P_MIN = None                            # Minimal active Power [W]
    P_MAX = None                            # Maximal active Power [W]
    P = None                                # Active Power (after control) [W]
    Q = None                                # Reactive Power (after control) [var]

    # result properties
    results = ['P', 'Q', 'P_MIN', 'P_MAX']

    # constructor
    def __init__(self, init_vals):
        """Initializes the mosaik-pv model.

        Args:
            init_vals (dict, optional): Initial values for creation of PV model.
        """

        # assign init values
        for i, (key, value) in enumerate(init_vals.items()):
            setattr(self, key, value)

        # convert time formats
        self.start_date = arrow.get(self.start_date, self.date_format)

        # open data file
        status = self.open()

        # check data format file
        self.check_data()

    # open file
    def open(self):
        """Opens the CSV file

        Returns:
            status (bool): Returns whether the file was opened successfully
        """

        # set status
        status = False

        # check if file exist
        if os.path.exists(self.datafile): 
            # load io datastream
            self.data = open(self.datafile)

            # set status
            status = True

        # return status
        return status                             

    # check data of file
    def check_data(self):
        """Checks if data file contains the simulation start date

        Raises:
            ValueError: Start date is not in CSV file
        """

        # jump over head lines
        for i in range(self.header_rows):
            self._read_next_row()    

        # Check start date
        self._read_next_row()
        if self.start_date < self.row_data[0]:
            raise ValueError('Start date "%s" not in CSV file.' %
                             self.start_date.format(self.date_format))
        while self.start_date > self.row_data[0]:
            self._read_next_row()
            if self.row_data is None:
                raise ValueError('Start date "%s" not in CSV file.' %
                                 self.start_date.format(self.date_format)) 

    # read next data row
    def _read_next_row(self):
        """Reads the next row of the CSV file into an array
        """

        try:
            self.row_data = next(self.data).strip().split(self.delimiter)
            self.row_data[0] = arrow.get(self.row_data[0], self.date_format)
        except StopIteration:
            self.row_data = None
        except:
            self.row_data = None                                                            

    # perform simulation step of household load
    def step(self, time):
        """Gets the active power from the next row of the CSV file 
        and calculates all other variables

        Args:
            time (int): Current simulation time
        Raises:
            IndexError: End of CSV file reached
        """

        # check if row_data empty
        if self.row_data is None:
            raise IndexError('End of CSV file reached.')

        # calculate expected date
        expected_date = self.start_date.shift(seconds=time*self.time_resolution)

        # check until right row
        while self.row_data[0] < expected_date:
            # Next time stamp
            self._read_next_row()

        # assign active/reactive power
        self.P_ACTUAL = float(self.row_data[1])*self.P_rated/self.P_rated_profile        

        # calculate P_MIN und P_MAX - control area
        self.P_MIN = 0
        self.P_MAX = self.P_ACTUAL

        # check set point and set P for grid calculation
        """P has to be positive value for Pandapower as the PV is modeled
        as a static generator using generator convention
        """
        if self.P_SET is None:
            # take actual active power
            self.P = abs(self.P_ACTUAL)
        else:
            # assign set point
            self.P = abs(self.P_SET)
        
        # calculation of reactive power
        self.Q = (1 - self.cos_phi) * self.P

        # Next time stamp
        self._read_next_row()

class Simulator(object):
    """Simulator class for mosaik-pv model
    Creates multiple instances and saves the results in an accesible list

    Args:
        object (class): Generic class
    """

    # contructor
    def __init__(self):
        """Constructor
        """

        # init data elments
        self.models = []
        self.results = []

    # create model instances
    def add_model(self, init_vals = None):
        """Create model instances of mosaik-pv

        Args:
            init_vals (dict): List (length=num) with initial values for each load model

        Returns:
            model: returns created load object
        """

        # create electric storage model
        model = PV(init_vals)

        # add model to model storage
        self.models.append(model)

        # add list for simulation data
        self.results.append([])

        # return model
        return model

    # perform simulation step of all models
    def step(self, time):
        """Performs simulation step of all to simulator connected models

        Args:
            time (int): Current simulation time.
        """
        
        # Enumaration over all models in simulator
        for i, model in enumerate(self.models):
            # perform simulation step
            model.step(time)

            # collect data of model and storage local
            for j, signal in enumerate(model.results):
                self.results[i].append(getattr(model,signal))
