# SmaBUI

SmaBUI is an easy to use co-simulation of a building energy system using mosaik. 

The following graphic shows a graphical overview of the total concept of SmaBUI. Please note that a complete setup will first be provided on mayor release.

![](docs/Co-Sim_SmaBUI.png)


#### Installation

1. Clone Repository `$ git clone https://gitlab.com/zdin-zle/scenarios/smabui.git`
2. Create virtual environment - see [installation](https://mosaik.readthedocs.io/en/latest/installation.html) guide of mosaik for help.
3. Activate virtual environment and install requirements `pip install -r requirements.txt`

#### Run Simulation
Executing the scenario_SmaBui.py starts the simulation of a one building including one PV, one battery storage, one charging station and one car.
