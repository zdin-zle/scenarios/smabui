"""@author: Henrik Wagner elenia@TUBS (henrik.wagner@tu-braunschweig.de)

Scenario file for the basic co-simulation scenario SmaBUI representing
one building with pv-system, battery storage, electric vehicle all controlled by an energy
management system and a standard household profile as load.


"""

# import packages
import mosaik, os, json
import mosaik.util
from datetime import datetime

# define help functions
def get_project_root():
    """Returns the path to the project root directory.

    Returns:
        str: A string with the project root directory
    """
    return os.path.realpath(os.path.dirname(__file__))


# Read & increase simulation counter - never set back to 0
def get_sim_counter(SIM_COUNTER_DIR):
    with open(SIM_COUNTER_DIR) as f:
        simulation_counter = json.load(f)
    simulation_counter["counter"] +=1
    with open(SIM_COUNTER_DIR, 'w') as outfile:
        json.dump(simulation_counter, outfile)
    return simulation_counter

# scenario selection
# print('Choose one of the following scenarios: SmaBui')
# scenario = input('type in the scenario: ')
filename = os.path.basename(__file__)
scenario = filename.split("scenario")[1].split(".py")[0]
#scenario = "SmaBui"

# define paths and filenames
BASE_DIR = get_project_root()
DATA_DIR = os.path.join(BASE_DIR, "data")
DATABASE_DIR = os.path.join(DATA_DIR, "database")
RESULTS_DIR = os.path.join(DATABASE_DIR,'results')
SIM_COUNTER_DIR = os.path.join(DATABASE_DIR, "simulation_counter.json")
MODEL_DATA_DIR = os.path.join(DATA_DIR,'model_data_scenario\\model_data'+scenario+'.json')
#GRIDFILE_DIR = os.path.join(DATA_DIR, "\grid\SmaBUI_Grid_Data\SmaBUI_demo_grid.json")
HDF5_FORMAT = datetime.now().strftime("%Y-%m-%d") + "_#" + str(get_sim_counter(SIM_COUNTER_DIR)['counter'])
DATABASE_FILENAME = os.path.join(RESULTS_DIR,"results_{}.hdf5".format(HDF5_FORMAT))

# check if RESULTS_DIR exists
if not os.path.exists(RESULTS_DIR):
    os.makedirs(RESULTS_DIR)

# check if MODEL_DATA_DIR is valid
if not os.path.isfile(MODEL_DATA_DIR):
    raise FileNotFoundError("Model data for "+scenario+" not found!")


# print paths
print("Selected model data: %s" %MODEL_DATA_DIR)
print("Results will be stored in %s." %DATABASE_FILENAME)

# Sim config. and other parameters
SIM_CONFIG = {
    'CSSim': {
        'python': 'components.charging_station.charging_station_simulator:Sim'
    },
    'CarSim': {
        'python': 'components.car.car_simulator:Sim'
    },
    'LoadSim': {
        'python': 'components.load.load_simulator:Sim'
    },
    'PVSim': {
        'python': 'components.pv.pv_simulator:Sim'
    },
    'StorageSim':{
       'python': 'components.storage.storage_simulator:StorageSimulator'
    },
    'ControlSim' :{
        'python': 'components.control.control_simulator:Sim'
    },
    'Pandapower': {
        'python': 'mosaik_pandapower.simulator:Pandapower'
    },
    'SignalGenerator': {
        'cmd': '%(python)s ' + os.path.join(BASE_DIR, 'components', 'data', 'signals.py') + ' %(addr)s'
    },
    'Collector': {
        'cmd': '%(python)s ' + os.path.join(BASE_DIR, 'components', 'data_collector', 'collector.py') + ' %(addr)s'
    },
    'DB': {
        'python': 'mosaik_hdf5:MosaikHdf5'
        },
}
# Configuration of simulation time and step size
days = 365
END = days * 24 * 60 * 60 # days * hours * minutes * seconds
STEP_SIZE = 900 

# Read Scenario file
with open(MODEL_DATA_DIR) as f:
    model_data = json.load(f)

# Overwrite simulation end from model_data.json with set in scenario.py
for i in model_data.keys():
    for j in range(len(model_data[i])):
        model_data[i][j]["END"] = END

# Create world
world = mosaik.World(SIM_CONFIG)

# Start simulators
LoadSim = world.start('LoadSim', step_size = STEP_SIZE)
PVSim = world.start('PVSim', step_size = STEP_SIZE)
StorageSim = world.start('StorageSim', step_size = STEP_SIZE)
# CarSim = world.start('CarSim', step_size = STEP_SIZE)
# CSSim = world.start('CSSim', step_size = STEP_SIZE)
ControlSim = world.start('ControlSim', step_size = STEP_SIZE)
DBSim = world.start('DB', step_size = STEP_SIZE, duration = END)

## INSTANTIATE

# Instantiate Database
database = DBSim.Database(filename=DATABASE_FILENAME)

# Instantiate Control Units
BEM = ControlSim.BEM.create(1) 

# # Instantiate Models CarSim
# cars = CarSim.car.create(len(model_data['car']), init_vals = model_data['car'])

# # Instantiate Models CSSim
# cs = CSSim.charging_station.create(len(model_data['charging_station']), init_vals = model_data['charging_station'])
# csEntities = CSSim.get_entities()

# Instantiate Models LoadSim
loads = LoadSim.household.create(len(model_data['load']), init_vals = model_data['load'])
loadsEntities = LoadSim.get_entities()

# Instantiate Models PVSim
pvs = PVSim.PV.create(len(model_data['pv']), init_vals = model_data['pv'])
pvsEntities = PVSim.get_entities()

# Instantiate Models StorageSim
storages = StorageSim.eStorage.create(1, init_vals = model_data['storage'])
storagesEntities = StorageSim.get_entities()

# Instantiate monitor/data collector 
#monitor = collector.Monitor()

# Instantiate Pandapower
# PPGridModel = GridSim.Grid(gridfile=GRIDFILE).children

# Get Pandapower grid elements
# PPbuses = [b for b in PPGridModel if b.type in 'Bus']
# PPloads = [l for l in PPGridModel if l.type in 'Load' and 'ext_load' not in l.eid]
# PPhouseLoad = [h for h in PPloads if 'house' in h.eid]
# PPstorageElements = [
#     s for s in PPGridModel if s.type in "Storage" and "battery" in s.eid]
# PPCSLoad = [c for c in PPloads if "charging" in c.eid]
# PPPVGen = [pv for pv in PPGridModel if pv.type in "Sgen" and "pv" in pv.eid]
# PPtrafo = [t for t in PPGridModel if t.type in "Trafo"]
# PPext_grid = [e for e in PPGridModel if e.type in "Ext_grid"]

# connect models to grid elements
# world.connect(loads[0], PPhouseLoad[0], ('P', 'p_mw'), async_requests=True)
# world.connect(storages[0], PPstorageElements[0], ('P', 'p_mw'), async_requests=True)
# world.connect(pvs[0], PPPVGen[0], ("P", 'p_mw'), async_requests=True)
# world.connect(cs[0], PPCSLoad[0], ("P", "p_mw"), async_requests=True)

# add components to BEM_APT
ControlSim.add_controlled_entity(BEM, loadsEntities)
ControlSim.add_controlled_entity(BEM, pvsEntities)
# ControlSim.add_controlled_entity(BEM, csEntities)
ControlSim.add_controlled_entity(BEM, storagesEntities)

# connect models to respective control
mosaik.util.connect_many_to_one(world, storages, BEM[0], ('P_DISCHARGE_MAX','P_MIN'),('P_CHARGE_MAX','P_MAX'), async_requests=True)
mosaik.util.connect_many_to_one(world, loads, BEM[0], ('P','P_LOAD'),('Q','Q_LOAD'), async_requests=True)
mosaik.util.connect_many_to_one(world, pvs, BEM[0], ('P_MIN','P_MIN'),('P_MAX','P_MAX'), ('P_ACTUAL', 'P_PV'), async_requests=True)
# mosaik.util.connect_many_to_one(world, cs, BEM[0], ('P_MIN','P_MIN'),('P_MAX','P_MAX'), ('P_SALDO_RES', 'P_SALDO_RES'),\
#     ('RES_SIGNAL', 'RES_SIGNAL'), async_requests=True)

# connect car with charging station
# for i, car in enumerate(cars):
#     if len(cs) - 1 >= i: 
#         world.connect(car, cs[i], 'appearance', 'E_BAT', 'E_BAT_MAX', 'P_DISCHARGE_MAX', 'P_CHARGE_MAX', 'STANDING_TIME', \
#             'STANDING_TIME_START', 'STANDING_TIME_END', 'TIME', 'ONE_MIN_PICKLE','NEXT_STEP_STANDING_MINUTES','NEXT_STEP_MINUTES', async_requests=True)

# connect models to database
mosaik.util.connect_many_to_one(world, pvs, database, 'P')
mosaik.util.connect_many_to_one(world, storages, database, 'SOC', 'P','P_SET')
mosaik.util.connect_many_to_one(world, loads, database, 'P')
# mosaik.util.connect_many_to_one(world, cs, database, 'P')
# mosaik.util.connect_many_to_one(world, cars, database, 'appearance','SOC')
mosaik.util.connect_many_to_one(world, BEM, database, 'P_SALDO')

# connect data collector to components
#mosaik.util.connect_many_to_one(world, cs, monitor, 'P', 'P_SET')
#mosaik.util.connect_many_to_one(world, cars, monitor, 'SOC')
#mosaik.util.connect_many_to_one(world, loads, monitor, 'P')
#mosaik.util.connect_many_to_one(world, pvs, monitor, 'P')
#mosaik.util.connect_many_to_one(world, storages, monitor, 'SOC')
#mosaik.util.connect_many_to_one(world, BEM, monitor, 'P_SALDO')
#mosaik.util.connect_many_to_one(world, PROFIT, monitor, 'P_SALDO_AEM')

# Run mosaik co-simulation
world.run(until=END)